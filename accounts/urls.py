from django.urls import path
from . import views

# views - 1. function based view
# 2. class based view

# accounts/
# accounts/myorder/

urlpatterns = [
    path("", views.myhome, name="myhome"),
    path("myorder", views.myorder, name="myorder")
]