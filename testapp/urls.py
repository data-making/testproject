from django.urls import path
from . import views

# views - 1. function based view
# 2. class based view

urlpatterns = [
    path("", views.home, name="home"),
    path("training/", views.training, name="training")
]